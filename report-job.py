
from pathlib import Path
import os, logging
import pandas as pd
from numerous.image_tools.job import NumerousBaseJob, ExitCode
from numerous.html_report_generator import Report, Section, GoFigure, DataFrameTable
from numerous.image_tools.app import run_job


logger = logging.getLogger('MyHTMLReportJob')


class NumerousReportJob(NumerousBaseJob):
 
    def run_job(self) -> ExitCode:
        # ...
        self.make_report(report)
        # ...
 
    def make_report(self, report: Report):
        raise NotImplementedError("Please implement the make_report method in your report job class")
    
class MyHTMLReportJob(NumerousReportJob):
 
    def make_report(self, report: Report):
        report.add_header_info(header='My Organization',
                       title='My First Report',
                       sub_title='but not the last',
                       sub_sub_title='at all',
                       footer_title='My first footer',
                       footer_content='This is the end!'
                       )


def run_example():
    run_job(numerous_job=MyHTMLReportJob(), appname="MyHTMLReportJob", model_folder=".")


if __name__ == "__main__":
    logging.basicConfig(level=logging.DEBUG)
    run_example()